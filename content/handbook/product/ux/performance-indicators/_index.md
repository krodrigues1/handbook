---

title: UX Department Performance Indicators
description: "Performance indicators for the UX department at GitLab"
---







<%= performance_indicators('UX Department') %>

<!-- To adjust the content on this page, you'll need to go to the accompanying yml page.  To find that, go to: www-gitlab-com - data - performance indicators - ux_department.yml-->
